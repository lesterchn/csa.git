import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    target: 'ESNext',
    minify: false,
    lib: {
      entry: './src/index.ts',
      name: 'Csa',
      fileName: 'csa',
      formats: ['es', 'umd']
    }
  }
})
