import { csa } from "./utils";
type CsaFrameEl = HTMLIFrameElement | HTMLFrameElement;
/** Csa 主站配置 */
interface CsaMasterOption {
    /** Csa 主站框架 */
    el?: string | CsaFrameEl;
    /** Csa 主站域名 --> targetOrigin */
    frameOrigin?: string;
}
/** Csa 主站访问器 */
declare class CsaMasterAccessor {
    private frameEl?;
    private readonly channel;
    private readonly targetOrigin;
    private readonly listens;
    private readonly returnHandlers;
    private exposeCtx;
    private status;
    set frame(el: string | CsaFrameEl);
    /** 设置导出函数 */
    set expose(ctx: Record<string, Function>);
    /** 主站发送对象 */
    private get port();
    /** 已连接 */
    get connected(): boolean;
    /**
     * Csa 主站构造器
     * @param config 访问器配置
     */
    constructor(config?: CsaMasterOption);
    /**
    * 调用主站导出方法
    * @param name 主站导出方法名称
    * @param args 主站导出方法参数值
    * @returns 主站导出方法调用返回值
    */
    call<T = any | void>(name: csa.CsaMasterCsaCallName, ...args: any[]): Promise<T>;
    /**
     * 发送/触发从站消息
     * @param destination 发送/触发目标
     * @param messagePayload 消息负载信息
     */
    emit(destination: csa.CsaMasterFrameEvent, ...messagePayload: any[]): void;
    /**
     * 监听/订阅主站消息
     * @param destination 订阅/监听目标
     * @param listener  订阅/监听消息处理函数
     * @param once 是否一次性，默认false,永久监听
     * @returns 停止订阅/监听处理函数
     */
    on(type: csa.CsaFrameEvent, listener: csa.CsaFrameListener, once?: boolean): csa.CsaStopFrameListenHandler;
    /**
     * 监听Csa是否已加载[允许多次监听]
     */
    ready(): Promise<void>;
    /**
     * 监听Csa是否已加载[允许多次监听]
     * @param handler 已加载的处理函数
     */
    ready(handler: csa.FrameReadyListener): void;
    /** 创建Csa跨站-从站对象 */
    static create(config?: CsaMasterOption): CsaMasterAccessor;
}
export default CsaMasterAccessor;
export { CsaMasterAccessor };
export declare const createMasterAccessor: typeof CsaMasterAccessor.create;
