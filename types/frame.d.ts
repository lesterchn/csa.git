import { csa } from "./utils";
/**
 * Csa从站访问器
 * @author lester<lieyunwuhe@sina.com>
 * @date 2022年12月03日
 */
declare class CsaFrameAccessor {
    /** 监听消息队列 */
    private readonly listens;
    /** 监听调用函数队列 */
    private readonly returnHandlers;
    /** 导出上下文 */
    private exposeCtx;
    /** 消息发送端 */
    private port;
    /** 设置导出函数 */
    set expose(ctx: Record<string, Function>);
    /** 是否已连接成功/或准备好了 */
    get connected(): boolean;
    constructor();
    /**
     * 调用主站导出方法
     * @param name 主站导出方法名称
     * @param args 主站导出方法参数值
     * @returns 主站导出方法调用返回值
     */
    call<T = any | void>(name: csa.CsaMasterCsaCallName, ...args: any[]): Promise<T>;
    /**
     * 发送/触发从站消息
     * @param destination 发送/触发目标
     * @param messagePayload 消息负载信息
     */
    emit(destination: csa.CsaMasterFrameEvent, ...messagePayload: any[]): void;
    /**
     * 监听/订阅主站消息
     * @param destination 订阅/监听目标
     * @param listener  订阅/监听消息处理函数
     * @param once 是否一次性，默认false,永久监听
     * @returns 停止订阅/监听处理函数
     */
    on(type: csa.CsaFrameEvent, listener: csa.CsaFrameListener, once?: boolean): csa.CsaStopFrameListenHandler;
    /**
     * 监听Csa是否已加载[允许多次监听]
     */
    ready(): Promise<void>;
    /**
     * 监听Csa是否已加载[允许多次监听]
     * @param handler 已加载的处理函数
     */
    ready(handler: csa.FrameReadyListener): void;
    /** 创建Csa跨站-从站对象 */
    static create(): CsaFrameAccessor;
}
export default CsaFrameAccessor;
export { CsaFrameAccessor };
export declare const createFrameAccessor: typeof CsaFrameAccessor.create;
