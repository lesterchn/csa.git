export declare namespace csa {
    type CsaFrameEvent = 'ready' | string;
    type CsaFrameListener = (...args: any[]) => void;
    type FrameReadyListener = () => void;
    type CsaStopFrameListenHandler = () => void;
    type CsaMasterFrameEvent = string;
    type CsaMasterCsaCallName = string;
    type CsaFrameCsaCallName = string;
    /** CSA 消息已连接类型 */
    type CsaReadyEvent = 'CSA_READY';
    /** CSA 消息发送类型 */
    type CsaEmitEvent = 'CSA_EMIT';
    /** CSA 消息调用类型 */
    type CsaCallEvent = 'CSA_CALL';
    /** CSA 消息调用返回类型 */
    type CsaCallReturnEvent = 'CSA_CALL_RETURN';
    /** CSA 消息类型 */
    type CsaEvent = CsaEmitEvent | CsaCallEvent | CsaCallReturnEvent;
    /** CSA 消息来源主站 */
    type CsaMasterSource = "CSA_MASTER";
    /** CSA 消息来源从站 */
    type CsaFrameSource = "CSA_FRAME";
    interface CsaReadyMessage {
        csaSource: CsaFrameSource;
        csaOrigin: string;
        csaEvent: CsaReadyEvent;
    }
    /** 调用消息 */
    interface CsaCallMessage {
        csaSource: CsaFrameSource;
        csaOrigin: string;
        csaEvent: CsaCallEvent;
        csaFnName: string;
        csaFnArgs: any[];
        csaId: string;
    }
    /** 触发/发送消息 */
    interface CsaEmitMessage {
        csaSource: CsaFrameSource;
        csaOrigin: string;
        csaEvent: CsaEmitEvent;
        csaEmitName: string;
        csaEmitArgs: any[];
    }
    /** 消息调用返回消息 */
    interface CsaCallReturnMessage<T = any> {
        csaSource: CsaFrameSource;
        csaOrigin: string;
        csaEvent: CsaCallReturnEvent;
        csaId: string;
        csaIsError?: boolean;
        csaReturnValue: T;
    }
    /** Csa 消息对象 */
    type CsaMessage = CsaEmitMessage | CsaCallMessage | CsaCallReturnMessage;
}
/** csa */
export declare class CsaUtils {
    /** CSA 消息已连接类型 */
    static readonly EVENT_READY = "CSA_READY";
    /** CSA 消息已连接类型 */
    static readonly EVENT_EMIT = "CSA_EMIT";
    /** CSA 消息已连接类型 */
    static readonly EVENT_CALL = "CSA_CALL";
    /** CSA 消息已连接类型 */
    static readonly EVENT_CALL_RETURN = "CSA_CALL_RETURN";
    /** CSA 从站消息来源 */
    static readonly SOURCE_FRAME = "CSA_FRAME";
    /** 下一帧执行函数 */
    static nextTick(handler: () => void | Promise<void>): void;
    /**
     * 尝试启用
     * @param handler 处理函数
     * @param args 处理参数
     * @param thisArg 处理上下文
     * @returns 处理函数返回值
     */
    static tryApply<T extends (...args: any[]) => void | Promise<void>>(handler: T, args?: any[], thisArg?: any): void | Promise<void>;
}
