# CsaAccessor iFrame/Frame 跨站/跨域访问器

## 初始化

```sh
yarn add @lesterchn/csa
npm i @lesterchn/csa
yarn add @lesterchn/csa
```

## 创建主站访问器

```ts
import { createMasterAccessor } from "@lesterchn/csa";

const master = createMasterAccessor({ el: "#frame" });

// 导出从站调用的方法
master.expose = {
  getMaster() {
    return `invoke getMaster`;
  },
  sayMaster(name: string) {
    return `master: ${name}`;
  },
};

// 连接成功后
master.ready(() => {
  console.log("slave 已准备好了");
  // 调用方法1
  console.log("调用getFrame返回值:", frame.call("getFrame"));
  // 调用方法2，并携带参数
  console.log("调用sayFrame返回值:", frame.call("sayFrame", "我是Master"));
  // 发送/推送普通消息
  frame.emit("frame:xxx", new Date().toLocaleDateString());
});

// 监听消息
master.on('master:xxx', (message: string) {
  console.log(message);
});

/** 监听一次性消息 */
master.on('master:xxx2', (message: string, args2: string) {
  /// todo
}, true)
```

## 创建从站访问器

```ts
import { createMasterAccessor } from "@lesterchn/csa";

const frame = createFrameAccessor();
// 到处主站调用的方法
frame.expose = {
  getFrame() {
    return `getSlave invoke`;
  },
  sayFrame(name: string) {
    return `frame: ${name}`;
  },
};

// 连接主站成功后回调
frame.ready(() => {
  console.log("slave 已准备好了");
  // 调用主站方法1
  console.log("调用getMaster返回值:", frame.call("getMaster"));
  // 调用主站方法2，并携带参数
  console.log("调用sayMaster返回值:", frame.call("sayMaster", "我是Frame"));
   // 发送/推送普通消息
  frame.emit("frame:xxx", new Date().toLocaleDateString());
});

// 监听消息
frame.on('frame:xxx', (message: string) {
  console.log(message);
});

/** 监听一次性消息 */
frame.on('frame:xxx2', (message: string, args2: string) {
  /// todo
}, true)
```

### 浏览器引用

```html
<script src="csa.umd.cjs"></script>
<script>
  const master = Csa.createMasterAccessor({ frame: "#frame" });
  // 导出方法
  master.expose = {
    getMaster() {
      return `invoke getMaster`;
    },
    sayMaster(name: string) {
      return `master: ${name}`;
    },
  };
</script>

<script>
  const frame = Csa.createFrameAccessor({});
  // 导出方法
  frame.expose = {
    getFrame() {
      return `getSlave invoke`;
    },
    sayFrame(name: string) {
      return `frame: ${name}`;
    },
  };
</script>
```
