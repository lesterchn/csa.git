

export namespace csa {
  export type CsaFrameEvent = 'ready' | string;
  export type CsaFrameListener = (...args: any[]) => void;
  export type FrameReadyListener = () => void;
  export type CsaStopFrameListenHandler = () => void;



  export type CsaMasterFrameEvent = string;
  export type CsaMasterCsaCallName = string;
  export type CsaFrameCsaCallName = string;

  /** CSA 消息已连接类型 */
  export type CsaReadyEvent = 'CSA_READY';
  /** CSA 消息发送类型 */
  export type CsaEmitEvent = 'CSA_EMIT';
  /** CSA 消息调用类型 */
  export type CsaCallEvent = 'CSA_CALL';
  /** CSA 消息调用返回类型 */
  export type CsaCallReturnEvent = 'CSA_CALL_RETURN';
  /** CSA 消息类型 */
  export type CsaEvent = CsaEmitEvent | CsaCallEvent | CsaCallReturnEvent;
  /** CSA 消息来源主站 */
  export type CsaMasterSource = "CSA_MASTER";
  /** CSA 消息来源从站 */
  export type CsaFrameSource = "CSA_FRAME";


  export interface CsaReadyMessage {
    csaSource: CsaFrameSource;
    csaOrigin: string;
    csaEvent: CsaReadyEvent;
  }

  /** 调用消息 */
  export interface CsaCallMessage {
    csaSource: CsaFrameSource;
    csaOrigin: string;

    csaEvent: CsaCallEvent;
    csaFnName: string;
    csaFnArgs: any[];
    csaId: string;
  }

  /** 触发/发送消息 */
  export interface CsaEmitMessage {
    csaSource: CsaFrameSource;
    csaOrigin: string;

    csaEvent: CsaEmitEvent;
    csaEmitName: string;
    csaEmitArgs: any[];
  }

  /** 消息调用返回消息 */
  export interface CsaCallReturnMessage<T = any> {
    csaSource: CsaFrameSource;
    csaOrigin: string;
    csaEvent: CsaCallReturnEvent;
    csaId: string;
    csaIsError?: boolean
    csaReturnValue: T
  }

  /** Csa 消息对象 */
  export type CsaMessage = CsaEmitMessage | CsaCallMessage | CsaCallReturnMessage;

}

/** csa */
export class CsaUtils {
  /** CSA 消息已连接类型 */
  public static readonly EVENT_READY = 'CSA_READY';
  /** CSA 消息已连接类型 */
  public static readonly EVENT_EMIT = 'CSA_EMIT';
  /** CSA 消息已连接类型 */
  public static readonly EVENT_CALL = 'CSA_CALL';
  /** CSA 消息已连接类型 */
  public static readonly EVENT_CALL_RETURN = 'CSA_CALL_RETURN';

  /** CSA 从站消息来源 */
  public static readonly SOURCE_FRAME = "CSA_FRAME";

  /** 下一帧执行函数 */
  public static nextTick(handler: () => void | Promise<void>) {
    Promise.resolve().then(() => handler());
  }

  /**
   * 尝试启用
   * @param handler 处理函数
   * @param args 处理参数
   * @param thisArg 处理上下文
   * @returns 处理函数返回值
   */
  public static tryApply<T extends (...args: any[]) => void | Promise<void>>(handler: T, args: any[] = [], thisArg?: any) {
    try {
      return handler.apply(thisArg, args);
    } catch (error) {
    }
  }
}